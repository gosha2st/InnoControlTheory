#ControlTheory - HomeWork1 - Computational background
#by Gosha Stepanov @gosha2st

from numpy.linalg import eigvals
from numpy import roots

#Finding the State Space model:
def StateSpace(coeffs):
    deg=len(coeffs)-1   #degree of a polynomial
    last_row=[]
    for i in range(deg, 0, -1):
        last_row.append(-coeffs[i]/coeffs[0])
    row=[]
    matrix=[]
    for _ in range(deg-1):
        row=[]
        for _ in range(deg):
            row.append(0)
        matrix.append(row)
    matrix.append(last_row)
    for i in range(deg-1):      #Putting identity matrix:
        matrix[i][i+1]=1
    return matrix

#Find poles of a Transfer function given ODE:
def Poles(coeffs):
    return roots(coeffs)

coeffs=(3.2316, 2.2279, 2.4488, 0.9344, 3.9760, 0, 1.8276)      #coefficients of initial ODE
matrix=StateSpace(coeffs)
print('Matrix A in State Space model:')
print(matrix)
print('\nEigenvalues of A:')
print(eigvals(matrix))
print('\nPoles of the Transfer function:')
print(Poles(coeffs))
